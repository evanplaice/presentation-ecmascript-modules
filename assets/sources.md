# Image Sources

phillip_lindsay.jpg

- http://www.dailycamera.com/sports/ci_32409902/phillip-lindsay-vows-return-pro-bowl-next-season

tc39_logo.png

- https://github.com/tc39

whatwg_logo.png

- https://whatwg.org/

nodejs_logo.png

- https://nodejs.org/en/

crying_happy_face.jpg

- https://megamailer.info/crying-meme-face/crying-meme-face-inspirational-40-sad-face-s/

are_we_there_yet.jpg

- https://www.batimes.com/robert-galen/definition-of-done-are-we-there-yet-part-1.html

angular_logo.png

- https://angular.io/presskit
