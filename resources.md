# Resources

NodeJS/Modules Working Group

- https://github.com/nodejs/modules

EcmaScript Modules Minimal Kernel Implementation

- https://github.com/nodejs/ecmascript-modules

Import File Specifiers

- https://github.com/GeoffreyBooth/node-import-file-specifier-resolution-proposal/

Syntax Disambiguation

- https://www.smotaal.io/markout/#experimental/modules/disambiguation/syntax

Dynamic Modules Specification

- https://nodejs.github.io/dynamic-modules/